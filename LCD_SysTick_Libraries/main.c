/**************************************************************************************
 * Author:       Trevor Ekin / Nabeeh Kandalaft
 * Course:       EGR 226
 * Program:      Example LCD Library usage
 * Description:  This main.c file is to serve as a simple validation for the LCD_Library.
 *                   The main while loop goes through all primary functions of the library
 *                   to demonstrate how to interface with the LCD.
 * **************************************************************************************/
#include "msp.h"
#include <stdint.h>
#include <stdio.h>
#include "LCD_Library.h"
#include "LCD_CustomImages.h"
#include "SysTick_Library.h"

int main(void)
{
    // disable watchdog timer
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

    // initialize LCD with defined pins (pins can be modified in LCD_Library.h)
    lcdInit();

    // initialize SysTick peripherals
    SysTickInit_NoInterrupts();

    // Custom characters are defined in LCD_CustomImages.h.  These defined layouts
    //      must be registered into the LCD's memory, shown below.

    // lcdCreateCustomChar takes a pointer to a custom character, like those created in
    //      LCD_CustomImages. It then returns a character-index of where that custom
    //      character was placed in the LCD's Character Generator RAM (CGRAM).


    // The custom character can then be printed to the screen using lcdSetChar, similarly
    //  to as normal characters, but using the custom character's index instead of an
    //  ASCII character. Refer to the data sheet for built-in characters on the LCD driver

    char bell_index     = lcdCreateCustomChar(&bell_layout);
    char stickfig_index = lcdCreateCustomChar(&stickfig_layout);
    char smile_index    = lcdCreateCustomChar(&smile_layout);


    while(1) {
        lcdClear();
        SysTick_delay_ms(500);             // delay second and a half

        //----------------------------------------------------------------------
        // TEST 1: lcdSetText on line 1
        //----------------------------------------------------------------------
        // lcdSetText takes three parameters: character string to print, x-coordinate and y-coordinate
        //     (int) x:      x-coordinate     x is the line number where line 1 is 0
        //     (int) y:      y-coordinate     y is the space from the beginning of the line

        lcdSetText("Sample Text",2,0);
        SysTick_delay_ms(500);             // delay second and a half



        //----------------------------------------------------------------------
        // TEST 2: lcdSetChar on line 2
        //----------------------------------------------------------------------
        // lcdSetChar takes three parameters: 1-character to print, x-coordinate and y-coordinate
        //     (int) x:      x-coordinate     x is the line number where line 1 is 0
        //     (int) y:      y-coordinate     y is the space from the beginning of the line

        lcdSetText("Char test: ",2,1);
        lcdSetChar('F',13,1);
        SysTick_delay_ms(500);             // delay second and a half



        //----------------------------------------------------------------------
        // TEST 3: lcdSetInt on line 3
        //----------------------------------------------------------------------
        // lcdSetInt takes three parameters: integer to print, x-coordinate and y-coordinate
        //     (int) x:      x-coordinate     x is the line number where line 1 is 0
        //     (int) y:      y-coordinate     y is the space from the beginning of the line

        lcdSetText("Int test: ",2,2);
        lcdSetInt(15,12,2);
        SysTick_delay_ms(500);             // delay second and a half



        //----------------------------------------------------------------------
        // TEST 4: lcdSetCharCustom on line 4
        //----------------------------------------------------------------------
        lcdSetText("Custom test: ",0,3);

        lcdSetChar(bell_index,    13,3);
        lcdSetChar(stickfig_index,14,3);
        lcdSetChar(smile_index,   15,3);

        SysTick_delay_ms(1500);             // delay second and a half


    }
}
